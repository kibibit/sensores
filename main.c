/*
*  Sensor de distancia 
*  con infrarrojo
*  contando tiempo
 */

#include <avr/io.h>
//#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include <stdlib.h>
#define F_CPU 8000000UL
#include <util/delay.h>

volatile uint8_t A,B,C,D,E;
volatile uint8_t sensores[6];
char buffer[3];

void iniUart(){
    UBRR0H = 0;
    UBRR0L = 51;
    UCSR0B = (1 << RXEN0) | (1 << TXEN0);  //habilita tx y rx,
    //UCSR0B |=  (1 << RXCIE0);   //interrupcion rx
    UCSR0C = (1 << UCSZ01) | (3 << UCSZ00); // formato 8n1
}
void Transmitir(uint8_t c){
    while (!(UCSR0A & (1<<UDRE0)));   //esperar a que el buffer se vacie
    UDR0 = c;                        //envia el dato
}
void TDato(char *d){
    while(*d) Transmitir(*d++);
} 
void Timers(){
    //configuracion Timer0 para 50uS
    OCR0A = 0x32;       //cuenta hasta 50uS
    TCCR0A = (1<<1);    //modo CTC
    TIFR0 = (1<<OCF0A);  //limpia bandera
    TIMSK0 = (1<<OCIE0A);//activa interrupcion
    TCCR0B = (1<<CS01); //clk/8
}
void Interrupciones(){
    PCMSK1 = (1<<PCINT8)|(1<<PCINT9)|(1<<PCINT10)|(1<<PCINT11)|(1<<PCINT12)|(1<<PCINT13);
}
void Sensores(){
    PCICR &= ~(1<<PCIE1);//desactiva interrupcion
    DDRC = 0xFF;    //puerto como salida
    PORTC = 0xFF;   //puerto en HIGH
    _delay_us(10);
    DDRC = 0;       //puerto como entrada
    A=0;
    PCIFR = (1<<PCIF1);  //limpia bandera
    PCICR |= (1<<PCIE1);//activa interrupcion
    D = 63;
}
void Enviar(){
    for(int i=0;i<6;i++){
        itoa(sensores[i],buffer,10);
        TDato(buffer);
        Transmitir(44); 
        sensores[i]=0;   
    }
    Transmitir(13);
    Transmitir(10);
}

int main(void){
    //DDRB |= (1<<PB1);
    MCUCR |= (1<<PUD);
    Timers();
    iniUart();
    Interrupciones();
    //sei();
    C=0;
    while(1){
        switch (C){
        case 0:
            PORTB |= (1<<PB1); //led enciende
            sei();
            Sensores();
            C=1;
            break;
        case 4:
            cli();
            Enviar();
            C=0;
            //_delay_ms(100);
            break;
        }
    }
    return 0;
}

ISR(TIMER0_COMPA_vect){
    A++;
    if (A==250) C=4;
}
ISR(PCINT1_vect){
    E=PINC;    
    B=D^E;
    D=E;E=0;
    while(B){
        E++;
        B>>=1;
    }
    sensores[E-1]=A;
    //B=A;
    //C++;
    //PORTB ^= (1<<PB1); //led se apaga
}